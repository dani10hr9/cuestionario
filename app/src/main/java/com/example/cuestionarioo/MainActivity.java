package com.example.cuestionarioo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    private CheckBox checkEspaña;
    private CheckBox checkInglaterra;
    private CheckBox checkMexico;

    private RadioButton radioSi;
    private RadioButton radioNo;
    private RadioButton radio3;
    private RadioButton radio4;
    private RadioButton radio90min;
    private RadioButton radio45min;

    private Switch switchPregunta5;

    private CheckBox check6_1;
    private CheckBox check6_2;
    private CheckBox checkUruguay;

    private CheckBox check7_1;
    private CheckBox check7_2;
    private CheckBox check7_3;

    private RadioButton radioBrasil;
    private RadioButton radioAlemania;
    private RadioButton radioArgentina;

    private RadioButton radio_2;
    private RadioButton radio_4;
    private RadioButton radio_6;

    private RadioButton radio_32;
    private RadioButton radio_28;

    private RadioButton radio_11;
    private RadioButton radio_8;

    private RadioButton radio12_1;
    private RadioButton radio12_2;

    private RadioButton radio13_1;
    private RadioButton radio13_2;

    private RadioButton radio14_1;
    private RadioButton radio14_2;


    private RadioButton radio15_1;
    private RadioButton radio15_2;

    private RadioButton radio16_1;
    private RadioButton radio16_2;

    private RadioButton radio17_1;
    private RadioButton radio17_2;

    private RadioButton radio18_1;
    private RadioButton radio18_2;

    private RadioButton radio19_1;
    private RadioButton radio19_2;

    private RadioButton radio20_1;
    private RadioButton radio20_2;

    private TextView txtTotal;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.checkInglaterra = findViewById(R.id.checkInglaterra);
        this.radioSi = findViewById(R.id.radioSi);
        this.radio3 = findViewById(R.id.radio3);
        this.radio90min = findViewById(R.id.radio90min);
        this.switchPregunta5 = findViewById(R.id.switchPregunta5);
        this.checkUruguay = findViewById(R.id.checkUruguay);
        this.check7_3 = findViewById(R.id.check7_3);
        this.radioBrasil = findViewById(R.id.radioBrasil);
        this.radio_4 = findViewById(R.id.radio_4);
        this.radio_32 = findViewById(R.id.radio_32);
        this.radio_11 = findViewById(R.id.radio_11);
        this.radio12_2 = findViewById(R.id.radio12_2);
        this.radio13_1 = findViewById(R.id.radio13_1);
        this.radio14_1 = findViewById(R.id.radio14_1);
        this.radio15_1 = findViewById(R.id.radio15_1);
        this.radio16_2 = findViewById(R.id.radio16_2);
        this.radio17_1 = findViewById(R.id.radio17_1);
        this.radio18_1 = findViewById(R.id.radio18_1);
        this.radio19_1 = findViewById(R.id.radio19_1);
        this.txtTotal = findViewById(R.id.txtTotal);


        checkInglaterra.setOnCheckedChangeListener(this);
        radioSi.setOnCheckedChangeListener(this);
        radio3.setOnCheckedChangeListener(this);
        radio90min.setOnCheckedChangeListener(this);
        switchPregunta5.setOnCheckedChangeListener(this);
        checkUruguay.setOnCheckedChangeListener(this);
        check7_3.setOnCheckedChangeListener(this);
        radioBrasil.setOnCheckedChangeListener(this);
        radio_4.setOnCheckedChangeListener(this);
        radio_32.setOnCheckedChangeListener(this);
        radio_11.setOnCheckedChangeListener(this);
        radio12_2.setOnCheckedChangeListener(this);
        radio13_1.setOnCheckedChangeListener(this);
        radio14_1.setOnCheckedChangeListener(this);
        radio15_1.setOnCheckedChangeListener(this);
        radio16_2.setOnCheckedChangeListener(this);
        radio17_1.setOnCheckedChangeListener(this);
        radio18_1.setOnCheckedChangeListener(this);
        radio19_1.setOnCheckedChangeListener(this);





    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        int var = 0, suma = 0;
        String resp;
        var += checkInglaterra.isChecked() ? 1 : 0;
        var += radioSi.isChecked() ? 1 : 0;
        var += radio3.isChecked() ? 1 : 0;
        var += radio90min.isChecked() ? 1 : 0;
        var += switchPregunta5.isChecked() ? 1 : 0 ;
        var += checkUruguay.isChecked() ? 1 : 0;
        var += check7_3.isChecked() ? 1 : 0;
        var += radioBrasil.isChecked() ? 1 : 0;
        var += radio_4.isChecked() ? 1 : 0;
        var += radio_32.isChecked() ? 1 : 0;
        var += radio_11.isChecked() ? 1 : 0;
        var += radio12_2.isChecked() ? 1 : 0;
        var += radio13_1.isChecked() ? 1 : 0;
        var += radio14_1.isChecked() ? 1 : 0;
        var += radio15_1.isChecked() ? 1 : 0;
        var += radio16_2.isChecked() ? 1 : 0;
        var += radio17_1.isChecked() ? 1 : 0;
        var += radio18_1.isChecked() ? 1 : 0;
        var += radio19_1.isChecked() ? 1 : 0;
        suma = suma + var;
        resp = String.valueOf(suma);
        txtTotal.setText("Resultados: " + resp);
    }
    public void calcular(View View)
    {
        txtTotal.setVisibility(android.view.View.VISIBLE);

    }
}
